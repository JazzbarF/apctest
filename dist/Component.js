sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device"
], function(UIComponent, Device) {
	"use strict";

	return UIComponent.extend("de.edk.rr.com.apc.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {

			// ggf. icon in Launchpad erzeugen
		/*	var oRenderer = sap.ushell.Container.getRenderer("fiori2");
			oRenderer.addHeaderEndItem(
				"sap.ushell.ui.shell.ShellHeadItem", {
					icon: "sap-icon://collision",
					tooltip: "Test",
					press: this.openTestPlugin.bind(this)
				}, true,
				false, [oRenderer.LaunchpadState.App, oRenderer.LaunchpadState.Home]);*/

			// Hier kannst du deinen Push Chanel einbinden...
			
			var uri 	= "wss://" + window.location.host + "/sap/bc/apc/sap/zmaint";
			var socket 	= new WebSocket(uri);
			socket.onopen = function(){
				console.log("Socket opened");
			};
			socket.onmessage = function(feed){
				if(feed.data !== undefined){
					console.log(feed.data);
				}
			};
			socket.onclose = function(){
				console.log("Socket closed");
			};
		},

		openTestPlugin: function(oEvent) {
			// Do stuff here, Mr. J
		}
	});
});